/**
 * SPDX-PackageName: kwaeri/mysql-migration-generator
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as assert from 'assert';
import { ServicePromiseBits } from '@kwaeri/service';
import { MysqlMigrationGenerator } from '../src/mysql-migration-generator.mjs';
import { Progress } from '@kwaeri/progress';
import * as _fs from 'node:fs/promises';
import debug from 'debug';


// DEFINES
let MIGRATION_ENV = process.env.NODE_ENV || "default";

const progress  = new Progress({ spinner: true, spinAnim: "dots", percentage: false }),
      migrationGenerator  = new MysqlMigrationGenerator(
                        MIGRATION_ENV !== "test" ? progress.getHandler() : undefined,
                        { environment: MIGRATION_ENV  } as any
                    );

const DEBUG = debug( 'kue:mysql-migration-generator-test' );

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );

    }
);


describe(
    'MySQLMigrationGenerator Functionality Test Suite',
    () => {

        describe(
            'Get Service Type Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal( migrationGenerator.serviceType, "Generator Service" )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Subscriptions Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( migrationGenerator.getServiceProviderSubscriptions() ),
                                JSON.stringify(
                                    {
                                        commands: { "add": { "migration": true } },
                                        required: { "add": { "migration": { /*"type": [ "mysql", "pg", "mongodb" ]*/ } } },
                                        optional: { "add": { "migration": { "skip-wizard": { "for": false, "flag": true }, "lang": { "for": false, "flag": false, "values": [ "typescript", "javascript" ] } } } }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Subscriptions Help Text Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( migrationGenerator.getServiceProviderSubscriptionHelpText() ),
                                 JSON.stringify(
                                    {
                                        helpText: {
                                            "commands": {   // List Help Text for our commands:
                                                "add": {    // ⇦ For add
                                                    "description": "The 'add' command automates content creation for an existing project.",
                                                    "specifications": { // ⇦ For the specifications
                                                        "migration": {  // ⇦ For migration
                                                            "description": "Adds a new empty migration of the type specified to the existing project, and according to options provided.",
                                                            "options": {    // ⇦ For the options of
                                                                "required": {   // ⇦ Required options are specific to the specification, 'migration' in this case
                                                                    "type": {   // ⇦ type is a required option
                                                                        "description": "Specify the template type to use when generating a migration for a project developed with @kwaeri/node-kit. Possible types include 'mysql', 'pg', 'mongodb'.",
                                                                        "values": {
                                                                            "mysql": {
                                                                                "desccription": "A mysql based migration."
                                                                            },
                                                                            "pg": {
                                                                                "description": "A postgreSQL based migration.",
                                                                            },
                                                                            "mongodb": {
                                                                                "description": "A mongodb based migration.",
                                                                            }
                                                                        }
                                                                    }
                                                                },
                                                                "optional": {   // ⇦ Optional options can be for the specification, or for the required options
                                                                    "specification": {  // ⇦ For the specification
                                                                        "language": {   // ⇦ List options
                                                                            "description": "Denotes the programming language for the migration being generated.",
                                                                            "values": [
                                                                                "typescript",
                                                                                "javascript"
                                                                            ]
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    },
                                                    "options": {    // ⇦ For the options of a command (where there isn't specifications)
                                                        "optional": {
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );

        describe(
            'Render Service Test',
            () => {
                it(
                    'Should create a MySQL migration in JavaScript.',
                    async () => {
                        if( MIGRATION_ENV !== "test" )
                            progress.init();

                        let testMethodResult;
                        /* The configuration passed in mocks that of a KwaeriConfiguration, which includes ProjectBits -
                           Don't let it scare you :D
                        */
                       try {

                            testMethodResult = ( await migrationGenerator.renderService( { quest: "add", specification: "migration", subCommands: ["AddTestTableToDatabase"], args: {}, version: "1.2.3" } ) as ServicePromiseBits );

                            if( testMethodResult && testMethodResult.result ) {
                                //let cleanUp = await filesystem.removeDirectory(.)
                                DEBUG( "In positive result for Render Service Test" );
                                //await _fs.rm( "data/migrations", { recursive: true, force: true } );
                            }
                       } catch( error ) {
                           console.error( error );
                       }

                        return Promise.resolve(
                            assert.equal( JSON.stringify( { type: testMethodResult?.type, result: testMethodResult?.result } ), JSON.stringify( { type: "add_mysql_migration", result: true } ) )
                        );
                    }
                );
            }
        );

    }
);